var express  = require('express');
var cors = require('cors');
var app = express();
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
app.use(cors());
var path = require("path");
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));
app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', "*");
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    res.header('Access-Control-Allow-Credentials', true);
    next();
});

app.get('/' , function (req , res) {
    res.sendStatus(200);
});

app.post('/checkcredentials' ,function (request,res) {

    var loginData = {
        username : request.username,
        securityToken : request.securityToken
    };
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    var qs = require("querystring");
    var http = require("https");

    var options = {
        "method": "POST",
        "hostname": "localhost",
        "port": 9090,
        "path": "/check",
        "headers": {
            "content-type": "application/json"
        }
    };
    var _req = http.request(options, function (_res , data ) {
        var chunks = [];
        _res.on("data", function (chunk) {
            chunks.push(chunk);

        });
        _res.on("end", function () {
            var body = Buffer.concat(chunks);
            res.send(body);
            console.log(body.toString() , "Answer for client side");
        });
    });
    _req.write(JSON.stringify({
        username: request.body.username,
        securityToken: request.body.securityToken
    }));
    // console.log(request.body.username);


    console.log(request.body,"----");

    _req.end();

    _req.on('error', function(err){
        console.log(JSON.stringify(err));
    });
});


app.listen(8080 , function () {
    console.log("Work")
});